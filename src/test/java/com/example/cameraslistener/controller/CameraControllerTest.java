package com.example.cameraslistener.controller;

import com.example.cameraslistener.dto.CameraDto;
import com.example.cameraslistener.model.UrlType;
import com.example.cameraslistener.service.CameraService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(CameraController.class)
@ExtendWith(SpringExtension.class)
class CameraControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    CameraService cameraService;

    @Autowired
    ObjectMapper mapper;

    @Test
    void listenToCameras() throws Exception {

        CameraDto cameraDto1 = new CameraDto(1L, UrlType.LIVE, "rtsp://127.0.0.1/1", "fa4b588e-249b-11e9-ab14-d663bd873d93", 120L);
        CameraDto cameraDto2 = new CameraDto(3L, UrlType.ARCHIVE, "rtsp://127.0.0.1/3", "fa4b5d52-249b-11e9-ab14-d663bd873d93", 120L);
        CameraDto cameraDto3 = new CameraDto(20L, UrlType.LIVE, "rtsp://127.0.0.1/20", "fa4b5f64-249b-11e9-ab14-d663bd873d93", 180L);
        CameraDto cameraDto4 = new CameraDto(2L, UrlType.ARCHIVE, "rtsp://127.0.0.1/2", "fa4b5b22-249b-11e9-ab14-d663bd873d93", 60L);
        List<CameraDto> dtoList = new ArrayList<>(Arrays.asList(cameraDto1, cameraDto2, cameraDto3, cameraDto4));

        when(cameraService.readData(any())).thenReturn(dtoList);

        MockHttpServletRequestBuilder with = post("/api/listen")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(""));

        mockMvc.perform(with)
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(mapper.writeValueAsString(dtoList)));

        verify(cameraService, times(1)).readData(any());
    }
}