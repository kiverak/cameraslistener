package com.example.cameraslistener.service;

import com.example.cameraslistener.dto.CameraDto;
import com.example.cameraslistener.mapper.CameraMapper;
import com.example.cameraslistener.model.Camera;
import com.example.cameraslistener.model.Source;
import com.example.cameraslistener.model.UrlType;
import com.example.cameraslistener.repository.CameraRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CameraServiceTest {

    @InjectMocks
    CameraService subj;

    @Mock
    CameraRepository cameraRepository;

    @Mock
    CameraMapper cameraMapper;

    @Test
    void readData() throws ExecutionException, InterruptedException {

        Source source1 = new Source(1L, "http://www.mocky.io/v2/5c51b230340000094f129f5d", "http://www.mocky.io/v2/5c51b5b6340000554e129f7b?mocky-delay=1s");
        List<Source> sources = new ArrayList<>(List.of(source1));

        Camera camera = new Camera(1L, UrlType.LIVE, "rtsp://127.0.0.1/1", "fa4b588e-249b-11e9-ab14-d663bd873d93", 120L);
        CameraDto cameraDto = new CameraDto(1L, UrlType.LIVE, "rtsp://127.0.0.1/1", "fa4b588e-249b-11e9-ab14-d663bd873d93", 120L);

        when(cameraRepository.save(any())).thenReturn(camera);
        when(cameraMapper.toDto(camera)).thenReturn(cameraDto);

        List<CameraDto> expectedList = new ArrayList<>(List.of(cameraDto));

        List<CameraDto> cameraDtoList = subj.readData(sources);

        Assertions.assertEquals(expectedList, cameraDtoList);
        verify(cameraRepository, times(1)).save(any());
        verify(cameraMapper, times(1)).toDto(camera);
    }
}