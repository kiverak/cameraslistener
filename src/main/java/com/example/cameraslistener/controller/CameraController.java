package com.example.cameraslistener.controller;

import com.example.cameraslistener.dto.CameraDto;
import com.example.cameraslistener.exception.CannotAccessSourceException;
import com.example.cameraslistener.model.Source;
import com.example.cameraslistener.service.CameraService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.net.URL;
import java.util.List;

import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequestMapping("/api")
public class CameraController {

    private final String URL_STRING = "http://www.mocky.io/v2/5c51b9dd3400003252129fb5";
    private final CameraService cameraService;

    public CameraController(CameraService cameraService) {
        this.cameraService = cameraService;
    }

    @PostMapping("/listen")
    public ResponseEntity<List<CameraDto>> listenToCameras() {

        ObjectMapper mapper = new ObjectMapper();
        List<Source> sources = null;
        try {
            sources = mapper.readValue(new URL(URL_STRING), new TypeReference<List<Source>>() {
            });
        } catch (IOException e) {
            throw new CannotAccessSourceException("Cannot access the source");
        }

        return ok(cameraService.readData(sources));
    }
}
