package com.example.cameraslistener;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CamerasListenerApplication {

    public static void main(String[] args) {
        SpringApplication.run(CamerasListenerApplication.class, args);
    }
}
