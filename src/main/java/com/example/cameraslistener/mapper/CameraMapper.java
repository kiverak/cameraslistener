package com.example.cameraslistener.mapper;

import com.example.cameraslistener.dto.CameraDto;
import com.example.cameraslistener.model.Camera;
import org.springframework.stereotype.Component;

@Component
public class CameraMapper {

    public Camera toEntity(CameraDto cameraDto) {

        return new Camera(cameraDto.getId(), cameraDto.getUrlType(), cameraDto.getVideoUrl(), cameraDto.getValue(), cameraDto.getTtl());
    }

    public CameraDto toDto(Camera camera) {

        return new CameraDto(camera.getId(), camera.getUrlType(), camera.getVideoUrl(), camera.getValue(), camera.getTtl());
    }
}
