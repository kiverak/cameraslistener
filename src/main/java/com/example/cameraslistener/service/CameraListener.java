package com.example.cameraslistener.service;

import com.example.cameraslistener.dto.CameraDto;
import com.example.cameraslistener.exception.SourceDataUnreachableException;
import com.example.cameraslistener.exception.SourceUnreachableException;
import com.example.cameraslistener.exception.TokenDataUnreachableException;
import com.example.cameraslistener.model.Source;
import com.example.cameraslistener.model.SourceData;
import com.example.cameraslistener.model.TokenData;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.concurrent.Callable;

public class CameraListener implements Callable<CameraDto> {

    private final Source source;

    public CameraListener(Source source) {
        this.source = source;
    }

    @Override
    public CameraDto call() {

        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request;
        HttpResponse<String> response;
        ObjectMapper mapper = new ObjectMapper();

        TokenData tokenData;
        try {
            request = HttpRequest.newBuilder()
                    .GET()
                    .header("accept", "application/json")
                    .uri(URI.create(source.getTokenDataUrl()))
                    .build();
            response = client.send(request, HttpResponse.BodyHandlers.ofString());
            tokenData = mapper.readValue(response.body(), new TypeReference<TokenData>() {
            });
        } catch (IOException | InterruptedException e) {
            throw new TokenDataUnreachableException(source.getId());
        }

        SourceData sourceData;
        try {
            request = HttpRequest.newBuilder()
                    .GET()
                    .header("accept", "application/json")
                    .uri(URI.create(source.getSourceDataUrl()))
                    .build();
            response = client.send(request, HttpResponse.BodyHandlers.ofString());
            sourceData = mapper.readValue(response.body(), new TypeReference<SourceData>() {
            });
        } catch (IOException | InterruptedException e) {
            throw new SourceDataUnreachableException(source.getId());
        }

        if (sourceData != null && tokenData != null) {
            return new CameraDto(source.getId(), sourceData.getUrlType(), sourceData.getVideoUrl(),
                    tokenData.getValue(), tokenData.getTtl());
        }

        throw new SourceUnreachableException(source.getId());
    }
}
