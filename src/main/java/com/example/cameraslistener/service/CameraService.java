package com.example.cameraslistener.service;

import com.example.cameraslistener.dto.CameraDto;
import com.example.cameraslistener.exception.CustomDatabaseException;
import com.example.cameraslistener.exception.SourceUnreachableException;
import com.example.cameraslistener.mapper.CameraMapper;
import com.example.cameraslistener.model.Camera;
import com.example.cameraslistener.model.Source;
import com.example.cameraslistener.repository.CameraRepository;
import lombok.AllArgsConstructor;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Service
@AllArgsConstructor
public class CameraService {

    private final CameraRepository cameraRepository;
    private final CameraMapper cameraMapper;

    @Transactional
    public List<CameraDto> readData(List<Source> sources) {

        ExecutorService exec = Executors.newCachedThreadPool();
        List<CameraDto> cameraDtoList = new ArrayList<>();

        for (Source source : sources) {
            try {
                CameraDto cameraDto = exec.submit(new CameraListener(source)).get();
                Camera camera = cameraRepository.save(cameraMapper.toEntity(cameraDto));
                CameraDto savedCameraDto = cameraMapper.toDto(camera);
                cameraDtoList.add(savedCameraDto);
            } catch (InterruptedException | ExecutionException e) {
                throw new SourceUnreachableException(source.getId());
            } catch (DataAccessException e) {
                throw new CustomDatabaseException("Ошибка доступа к базе данных");
            }
        }

        return cameraDtoList;
    }
}
