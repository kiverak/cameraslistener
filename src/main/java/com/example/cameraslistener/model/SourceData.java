package com.example.cameraslistener.model;

import lombok.Data;

@Data
public class SourceData {
    private UrlType urlType;
    private String videoUrl;
}
