package com.example.cameraslistener.model;

import lombok.Data;

@Data
public class TokenData {
    private String value;
    private Long ttl;
}
