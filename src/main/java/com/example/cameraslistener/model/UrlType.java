package com.example.cameraslistener.model;

public enum UrlType {
    LIVE, ARCHIVE;
}
