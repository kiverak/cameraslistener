package com.example.cameraslistener.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Source {
    private Long id;
    private String sourceDataUrl;
    private String tokenDataUrl;
}
