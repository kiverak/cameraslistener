package com.example.cameraslistener.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "camera")
public class Camera {

    @Id
    private Long id;

    @Column(name = "urlType")
    private UrlType urlType;

    @Column(name = "videoUrl")
    private String videoUrl;

    @Column(name = "token")
    private String value;

    @Column(name = "ttl")
    private Long ttl;
}
