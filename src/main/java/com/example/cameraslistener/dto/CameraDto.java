package com.example.cameraslistener.dto;

import com.example.cameraslistener.model.UrlType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CameraDto {

    private Long id;
    private UrlType urlType;
    private String videoUrl;
    private String value;
    private Long ttl;
}
