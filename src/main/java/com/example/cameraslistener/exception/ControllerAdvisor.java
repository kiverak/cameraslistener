package com.example.cameraslistener.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.Map;

public class ControllerAdvisor extends ResponseEntityExceptionHandler {

    @ExceptionHandler(CameraNotFoundException.class)
    public ResponseEntity<Object> handleCameraNotFoundException(CameraNotFoundException ex) {

        Map<String, Object> body = new LinkedHashMap<>();
        body.put("timestamp", LocalDateTime.now());
        body.put("message", ex.getMessage());

        return new ResponseEntity<>(body, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(CustomDatabaseException.class)
    public ResponseEntity<Object> handleCustomDatabaseException(CustomDatabaseException ex) {

        Map<String, Object> body = new LinkedHashMap<>();
        body.put("timestamp", LocalDateTime.now());
        body.put("message", ex.getMessage());

        return new ResponseEntity<>(body, HttpStatus.SERVICE_UNAVAILABLE);
    }

    @ExceptionHandler(CannotAccessSourceException.class)
    public ResponseEntity<Object> handleCannotAccessSourceException(CannotAccessSourceException ex) {

        Map<String, Object> body = new LinkedHashMap<>();
        body.put("timestamp", LocalDateTime.now());
        body.put("message", ex.getMessage());

        return new ResponseEntity<>(body, HttpStatus.SERVICE_UNAVAILABLE);
    }

    @ExceptionHandler(SourceUnreachableException.class)
    public void handleSourceUnreachableException(SourceUnreachableException ex) {

        // TODO send msg to logger
        System.out.println(ex.getMessage());
    }

    @ExceptionHandler(SourceDataUnreachableException.class)
    public void handleSourceDataUnreachableException(SourceDataUnreachableException ex) {

        // TODO send msg to logger
        System.out.println(ex.getMessage());
    }

    @ExceptionHandler(TokenDataUnreachableException.class)
    public void handleTokenDataUnreachableException(TokenDataUnreachableException ex) {

        // TODO send msg to logger
        System.out.println(ex.getMessage());
    }
}
