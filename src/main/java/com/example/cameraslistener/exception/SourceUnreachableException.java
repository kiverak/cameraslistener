package com.example.cameraslistener.exception;

public class SourceUnreachableException extends RuntimeException {
    public SourceUnreachableException(Long id) {
        super(String.format("Camera source %d unreachable", id));
    }
}
