package com.example.cameraslistener.exception;

public class SourceDataUnreachableException extends RuntimeException {
    public SourceDataUnreachableException(Long id) {
        super(String.format("Camera source data %d unreachable", id));
    }
}
