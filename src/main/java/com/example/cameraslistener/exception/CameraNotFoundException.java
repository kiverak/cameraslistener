package com.example.cameraslistener.exception;

public class CameraNotFoundException extends RuntimeException{

    public CameraNotFoundException(Long id) {
        super(String.format("Camera with Id %d not found", id));
    }
}
