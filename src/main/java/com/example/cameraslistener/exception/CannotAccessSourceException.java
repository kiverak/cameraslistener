package com.example.cameraslistener.exception;

public class CannotAccessSourceException extends RuntimeException {
    public CannotAccessSourceException(String message) {
        super(message);
    }
}
