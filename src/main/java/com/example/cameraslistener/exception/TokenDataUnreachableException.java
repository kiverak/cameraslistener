package com.example.cameraslistener.exception;

public class TokenDataUnreachableException extends RuntimeException {
    public TokenDataUnreachableException(Long id) {
        super(String.format("Camera token data %d unreachable", id));
    }
}
