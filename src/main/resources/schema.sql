create table camera (
    id bigserial primary key,
    urlType varchar(255) NOT NULL,
    videoUrl varchar(255) NOT NULL,
    token varchar(255) NOT NULL,
    ttl bigint NOT NULL
);